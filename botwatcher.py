from disco.bot import Plugin
from db import *
import disco.util.snowflake as snowflake
from datetime import timedelta, datetime

from disco.types.permissions import Permissible, PermissionValue, Permissions


class BotWatcher(Plugin):
	def __init__(self, bot, config):
		Plugin.__init__(self, bot=bot, config=config)
		if not self.client.config.configured_db:
			setupDb(self.client.config.db)

	def isMod(self, member, guild):
		return guild.get_permissions(member).can(Permissions.MANAGE_ROLES.value)

	def getExistingOrEmpty(self, collection, id, guild_id):
		if collection.exists(id=id, guild_id=guild_id):
			return collection.get(id=id, guild_id=guild_id)
		else:
			return collection(id=id, guild_id=guild_id)

	def getObjectOrNone(self, collection, id, guild_id):
		if collection.exists(id=id, guild_id=guild_id):
			return collection.get(id=id, guild_id=guild_id)
		else:
			return None

	def getSetting(self, collection, guild_id):
		if collection.exists(guild_id=guild_id):
			return collection.get(guild_id=guild_id)
		else:
			return None

	def addToCollection(self, collection, id, event):
		if not self.isMod(event.author, event.guild):
			return
		self.getExistingOrEmpty(collection, id, event.guild.id)
		commit()

	def removeFromCollection(self, collection, id, event):
		if not self.isMod(event.author, event.guild):
			return

		obj = self.getObjectOrNone(collection, id, event.guild.id)
		if (obj != None):
			obj.delete()
			commit()
			return True
		return False

	@db_session
	@Plugin.command('setalertchannel')
	def setAlertChannel(self, event):
		if not self.isMod(event.author, event.guild):
			return
		delete(alertChannel for alertChannel in AlertChannel if alertChannel.guild_id == event.guild.id)
		commit()
		self.addToCollection(AlertChannel, event.channel.id, event)
		event.msg.reply("Set")

	def getSetting(self, collection, guild_id):
		if collection.exists(guild_id=guild_id):
			return collection.get(guild_id=guild_id)
		else:
			return None

	@db_session
	@Plugin.listen('MessageDelete')
	def messageDelete(self, event):
		selectedMsg = snowflake.to_datetime(snowflake.to_snowflake(int(event.id)))
		weekAgo = datetime.now() - timedelta(days=7)
		if not weekAgo < selectedMsg:
			alertChan = self.getSetting(AlertChannel, event.guild.id)
			if alertChan is None:
				return
			event.guild.channels[alertChan.id].send_message("Old message deleted in <#%d>, id: %d - %s" %
					(event.channel.id, event.id,
					snowflake.to_datetime(event.id)))

	@db_session
	@Plugin.command('addwatcheduser', '<id:int>')
	def addWatchedUser(self, event, id):
		self.addToCollection(WatchedUser, id, event)
		event.msg.reply('Done')

	@db_session
	@Plugin.command('removewatcheduser', '<id:int>')
	def removeWatchedUser(self, event, id):
		self.removeFromCollection(WatchedUser, id, event)
		event.msg.reply('Done')
