import unittest
import sys

from disco.client import ClientConfig, Client
from disco.bot.bot import Bot
from disco.types.permissions import Permissions, PermissionValue
import disco.util.snowflake as snowflake

from datetime import timedelta, datetime


sys.path.append('..')

from pony.orm import *

import botwatcher
import db

class MockBot(Bot):
    @property
    def commands(self):
        return getattr(self, '_commands', [])

class Object(object):
	pass

class Guild:
	def __init__(self, id, name, members, roles, channels):
		self.id = id
		self.name = name
		self.members = members
		self.roles = roles
		self.channels = channels
		self.audit = []
	def get_audit_log_entries(self):
		return self.audit
	def get_permissions(self, user):
		if 1 in user.roles:
			return PermissionValue(Permissions.MANAGE_ROLES.value)
		else:
			return PermissionValue(Permissions.SPEAK.value)
	def setAudit(self, audit):
		self.audit = audit

class Channel:
	def __init__(self, id, name):
		self.id = id
		self.name = name
		self.msg = None
		self.embed = None
	def send_message(self, msg=None, embed=None):
		self.msg = msg
		self.embed = embed

class Member:
	def __init__(self, id, name, roles = []):
		self.id = id
		self.name = name
		self.roles = roles
		self.removed_roles = []
		self.added_roles = []
	def __str__(self):
		return self.name
	def get_scan_variants(self):
		return [str(self.id), self.name]
	def setGuild(self, guild):
		self.guild = guild
		self.guild_id = guild.id
	def remove_role(self, id):
		self.removed_roles.append(id)
	def add_role(self, id):
		self.added_roles.append(id)

class Event(object):
	def __init__(self, author, channel, guild):
		self.channel = channel
		self.rep = None
		self.embed = None
		self.guild = guild
		self.msg = Object
		self.msg.reply = self.reply
		self.member = self.author = author
	def reply(self, rep=None, embed=None):
		self.rep = rep
		self.embed = embed

class GuildEvent(object):
	def __init__(self, member, guild, id, channel):
		self.member = member
		self.guild = guild
		self.id = id
		self.channel = channel
        
class Role:
	def __init__(self, id, name):
		self.id = id
		self.name = name

def getIdDict(l):
	ret = {}
	for el in l:
		ret[el.id] = el
	return ret

class BotWacherTests(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		db.setupDb("test.sqlite")
	def setUp(self):
		self.config = ClientConfig(
			{'config': 'TEST_TOKEN', 'configured_db': True}
		)
		self.client = Client(self.config)
		self.bot = MockBot(self.client)
		self.plugin = botwatcher.BotWatcher(self.bot, self.config)
		self.setupStructure()
	
	def tearDown(self):
		db.db.drop_all_tables(with_all_data=True)
		db.db.create_tables()
		
	@db_session	
	def setupStructure(self):
		self.member1 = Member(600, "m1", roles=[1])
		self.member2 = Member(700, "m2", roles=[])

		self.good_channel = Channel(13, "c1")
		self.another_channel = Channel(14, "c2")
		role_dict = {}
		self.good_guild = Guild(6, "g6", getIdDict([self.member1, self.member2]), role_dict, getIdDict([self.another_channel, self.good_channel]))

		self.member1.setGuild(self.good_guild)
		self.member2.setGuild(self.good_guild)
		
		self.plugin.state.guilds = {self.good_guild.id : self.good_guild }

	def getGoodEvent(self):
		return Event(self.member1, self.good_channel, self.good_guild)
	
	def getGoodEventOtherChannel(self):
		return Event(self.member1, self.another_channel, self.good_guild)
	
	def getBadUserEvent(self):
		return Event(self.member2, self.good_channel, self.good_guild)
	
	def getTestSet(self):
		return [{"add": self.plugin.addWatchedUser, "collection": db.WatchedUser, "remove": self.plugin.removeWatchedUser},
		]
	
	@db_session
	def test_set_alert_channel_set(self):
		self.plugin.setAlertChannel(self.getGoodEvent())
		self.plugin.messageDelete(GuildEvent(self.member2, self.good_guild, 1, self.good_channel))

		self.assertIsNotNone(self.good_channel.msg)
		self.assertTrue("deleted" in self.good_channel.msg)

	@db_session
	def test_set_alert_channel_change(self):
		self.plugin.setAlertChannel(self.getGoodEvent())
		self.plugin.setAlertChannel(self.getGoodEventOtherChannel())
		self.plugin.messageDelete(GuildEvent(self.member2, self.good_guild, 1, self.good_channel))

		self.assertIsNotNone(self.another_channel.msg)
		self.assertTrue("deleted" in self.another_channel.msg)

	@db_session
	def test_set_alert_channel_block(self):
		self.plugin.setAlertChannel(self.getBadUserEvent())
		self.plugin.messageDelete(GuildEvent(self.member2, self.good_guild, 1, self.good_channel))
		self.assertIsNone(self.good_channel.msg)

	@db_session
	def test_set_alert_older_than_1_week(self):
		self.plugin.setAlertChannel(self.getGoodEvent())
		self.plugin.messageDelete(GuildEvent(self.member2, self.good_guild, snowflake.from_datetime(datetime.now() - timedelta(days=8)), self.good_channel))

		self.assertIsNotNone(self.good_channel.msg)
		self.assertTrue("deleted" in self.good_channel.msg)


	@db_session
	def test_set_alert_newer_than_1_week(self):
		self.plugin.setAlertChannel(self.getGoodEvent())
		self.plugin.messageDelete(GuildEvent(self.member2, self.good_guild, snowflake.from_datetime(datetime.now() - timedelta(days=6)), self.good_channel))

		self.assertIsNone(self.good_channel.msg)

		
		
		
if __name__ == '__main__':
	unittest.main()
