from pony.orm import *

db = Database()

class WatchedUser(db.Entity):
	id = PrimaryKey(int, size=64)
	guild_id = Required(int, size=64)

class AlertChannel(db.Entity):
	guild_id = PrimaryKey(int, size=64)
	id = Required(int, size=64) # channel

def setupDb(name):
	set_sql_debug(False)
	db.bind(provider='sqlite', filename=name, create_db=True)
	db.generate_mapping(create_tables=True)
	commit()

